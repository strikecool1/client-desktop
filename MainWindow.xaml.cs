﻿using AlphaPersonel.Theme.Extensions;
using System.Windows;
using System.Windows.Media;

namespace AlphaPersonel
{
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();
            if (TryFindResource("AccentColorBrush") is SolidColorBrush accentBrush)
            {
                accentBrush.Color.CreateAccentColors();
            }
        }
    }
}
