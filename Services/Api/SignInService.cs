﻿using AlphaPersonel.Models;
using System.Configuration;
using System.IO;
using System.Net;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace AlphaPersonel.Services.Api
{
    internal class SignInService
    {

        private static readonly string? _ApiUrl = ConfigurationManager.AppSettings["api"];

        #region Авторизация пользователя
        public static async ValueTask<Users> SignIn(string userName, string password)
        {
            HttpWebRequest req = (HttpWebRequest)WebRequest.Create(_ApiUrl + "/auth");// Создаём запрос
            req.Method = "Post";
            req.Accept = "application/json";

            await using (StreamWriter streamWriter = new(req.GetRequestStream()))
            {
                req.ContentType = "application/json";
                // Второй параметр ключ , оно должно совпадать с ключом на сервере
                string encryptedPass = CustomAes256.Encrypt(password, "8UHjPgXZzXDgkhqV2QCnooyJyxUzfJrO");
                Users user = new()
                {
                    UserName = userName,
                    Password = encryptedPass,
                    IdModules = ModulesProject.Personel
                };
                string json = JsonSerializer.Serialize(user);
                await streamWriter.WriteAsync(json);
                // Записывает тело
                streamWriter.Close();
            }
            using WebResponse response = await req.GetResponseAsync();
            await using Stream responseStream = response.GetResponseStream();
            using StreamReader reader = new(responseStream, Encoding.UTF8);
            #pragma warning disable CS8603 // Возможно, возврат ссылки, допускающей значение NULL.
            return JsonSerializer.Deserialize<Users>(await reader.ReadToEndAsync());
            #pragma warning restore CS8603 // Возможно, возврат ссылки, допускающей значение NULL.
        }
        #endregion
    }
}
