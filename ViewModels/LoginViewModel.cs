﻿using AlphaPersonel.Commands;
using AlphaPersonel.Models;
using AlphaPersonel.Services;
using AlphaPersonel.Services.Api;
using System.IO;
using System.Net;
using System.Text.Json;
using System.Windows;
using System.Windows.Input;

namespace AlphaPersonel.ViewModels
{
    internal class LoginViewModel : BaseViewModel
    {
        public LoginViewModel(NavigationStore navigationStore)
        {
            this.navigationStore = navigationStore;
        }

        #region Свойство
        private readonly NavigationStore navigationStore;
        // Введённый логин
        private string? _UserName;
        public string? UserName
        {
            get => _UserName;
            set => Set(ref _UserName, value);
        }
        // Введённый Пароль
        private string? _Password;
        public string? Password
        {
            get => _Password;
            set => Set(ref _Password, value);
        }
        // Сообщение ошибки
        private string? _ErrorMessage;
        public string? ErrorMessage
        {
            get => _ErrorMessage;
            private set
            {
                _ = Set(ref _ErrorMessage, value);
                OnPropertyChanged(nameof(IsErrorVisible));
            }
        }
        // Свойство отображения уведомления при не валидных данных 
        public bool IsErrorVisible => !string.IsNullOrEmpty(ErrorMessage);
        // Доступность
        private bool CanSignIn(object parameter)
        {
            return !string.IsNullOrEmpty(UserName) && !string.IsNullOrEmpty(Password);
        }
        #endregion

        #region Команды

        // Команда Авторизации
        private ICommand? _SignIn;
        // Лямбда Команда 
        public ICommand SignIn => _SignIn ??= new LambdaCommand(OnSignInAsync, CanSignIn);
        #endregion

        #region Логика Команд

        // Логика Авторизации
        private async void OnSignInAsync(object p)
        {
            Users account;

            try
            {
                account = await SignInService.SignIn(userName: UserName!, password: Password!);

                if (account.StatusCode == HttpStatusCode.OK)
                {
                    navigationStore.CurrentViewModel = new HomeViewModel(account, navigationStore);
                }
            }
            catch (WebException ex)
            {

                if (ex.Status == WebExceptionStatus.ProtocolError)
                {
                    if (ex.Response is HttpWebResponse response)
                    {
                        using StreamReader reader = new(response.GetResponseStream());
                        if (reader != null)
                        {

#pragma warning disable CS8600 // Преобразование литерала, допускающего значение NULL или возможного значения NULL в тип, не допускающий значение NULL.
                            account = JsonSerializer.Deserialize<Users>(await reader.ReadToEndAsync());
#pragma warning restore CS8600 // Преобразование литерала, допускающего значение NULL или возможного значения NULL в тип, не допускающий значение NULL.
                            ErrorMessage = account!.Error!.Ru;
                        }
                    }
                }
                else
                {
                    _ = MessageBox.Show("Не удалось получить данные с API!", "Fatal Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }

        }
        #endregion
        public override void Dispose()
        {

            base.Dispose();
        }
    }
}
