﻿using System.Text.Json.Serialization;


namespace AlphaPersonel.Models.PersonCard
{
    internal class TypePassport
    {
        [JsonPropertyName("id")]
        public int Id { get; set; }
        [JsonPropertyName("name")]
        public string? Name { get; set; }
    }
}
