﻿using System.Text.Json.Serialization;

namespace AlphaPersonel.Models
{
    internal class ErrorLanguage
    {
        [JsonPropertyName("ru")]
        public string Ru { get; set; } = string.Empty;
    }
}
