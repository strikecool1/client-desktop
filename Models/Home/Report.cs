﻿
namespace AlphaPersonel.Models
{
    internal class Report
    {
        public string? Name { get; set; }
        public string? Url { get; set; }
    }
}
