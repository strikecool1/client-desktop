﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace AlphaPersonel.Models
{
    internal class Position
    {
        [JsonPropertyName("id")]
        public int Id { get; set; }
        // Рабочий телефон
        [JsonPropertyName("phone")] public string Phone { get; set; } = string.Empty;
        // Навзание должности
        [JsonPropertyName("position")] public string Name { get; set; } = string.Empty;
        //TODO: Потом поменяй на объект в API чтобы можно было через combobox выбирать отдел
        [JsonPropertyName("name_depart")] public string DepartmentName { get; set; } = string.Empty;

        // Является ли должность педагогической
        [JsonPropertyName("is_ped")]
        public bool IsPed { get; set; }

        // Основная ли должность
        [JsonPropertyName("is_main")]
        public bool IsMain { get; set; }

        // Сколько дней отпуска по должности
        [JsonPropertyName("holiday")]
        public int? HolidayLimit { get; set; }

        [JsonPropertyName("stavka_nobudget")]
        public decimal? StavkaNoBudget { get; set; }

        [JsonPropertyName("stavka_budget")]
        public decimal? StavkaBudget { get; set; }

        private decimal _CountAllBudget;

        public decimal CountAllBudget
        {
            get => _CountAllBudget = Count_B + Count_NB;
            set => _CountAllBudget = value;
        }

        private decimal _CountAllFreeBudget;

        public decimal CountAllFreeBudget
        {
            get => _CountAllFreeBudget = Free_B + Free_NB;
            set => _CountAllFreeBudget = value;
        }

        private decimal _CountOklad;

        public decimal CountOklad
        {
            get => _CountOklad = Oklad_B + Oklad_NB;
            set => _CountOklad = value;
        }

        [JsonPropertyName("count_budget")]
        public decimal Count_B { get; set; } = 0;

        [JsonPropertyName("count_nobudget")]
        public decimal Count_NB { get; set; } = 0;

        [JsonPropertyName("free_budget")]
        public decimal Free_B { get; set; }

        [JsonPropertyName("free_nobudget")]
        public decimal Free_NB { get; set; }

        [JsonPropertyName("oklad_budget")]
        public decimal Oklad_B { get; set; }

        [JsonPropertyName("oklad_nobudget")]
        public decimal Oklad_NB { get; set; }

        [JsonPropertyName("priority")]
        public short Priority { get; set; }
    }
}
